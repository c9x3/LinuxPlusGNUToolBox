# LinuxPlusGNUToolBox v1.2.4.6

v1.2.4.6 is all about Code Cleanup!

- I've removed a couple of redundant and unnecessary things such as out of place comments (previously in 'Unit Converter Menu Function' and 'Temperature Unit Converter Menu Function') and also removed a placeholder function (an earlier version of 'fn_time_unit_converter_menu.')
- In order to prevent conflicts with the working version and reduce bloat, the placeholder function of 'fn_time_unit_converter_menu' was removed!
- Updated readme.
- Corrected spelling mistake in readme.
- Found a bug and acknowledging it in the readme.

# LinuxPlusGNUToolBox v1.2.4.5

- Added a new tool! '12-24 Hour Time Table' which displays a 12 and 24 hour Time Table! :)
- I don't know if I fixed the Updater Bug but I do think I was able to fix the issue in this patch.
- Updated Manual!
- Updated Readme!

# LinuxPlusGNUToolBox v1.2.4.4

Another big update with some changes!

- A new tool! 'Temperature Converter!' You can now accurately convert Celsius To Fahrenheit or Fahrenheit To Celsius!
- Change: 'Time (UNIT) Converter' has been moved to within 'Unit Converter Menu' and 'Unit Converter Menu' now takes the spot 'Time (UNIT) Converter' previously had.
- New Manual Entry!
- Updated Manual!
- Updated Readme!
- Found a bug and acknowledging it in the readme.

# LinuxPlusGNUToolBox v1.2.4.3

This has been a big update! A new manual entry, updated manual, a new tool (a random password generator.) Fixed a bug etc.

- A new tool! A Random Password Generator!
- New Manual Entry for Password Generator!
- Updated Manual!
- Updated Readme!
- Hopefully resolved a bug where the updater would make the copy of the software before updating and then delete the copy before updating.
- Instead of having a 3 second countdown, now its 1 second to try to syncronise timezones in worldtime (this is giving it time to sync just encase it needs it.)
- Removed donation info from the software. If you want to donate, (thank you) please use the link provided in the manual (Licensing and Donation Information
) or in the readme. 😃

# LinuxPlusGNUToolBox v1.2.4.2

- No specific donation amount anymore.

# LinuxPlusGNUToolBox v1.2.4.1

- Added START and END comment to most (if not all) functions so its easier to work with the code.
- Added 3 line gap between each function so its easier to work with the code.
- Kept new information upon finishing Organizer from v1.2.3.8.

### I decided to revert Organizer back to v1.2.3.7 because:

- v1.2.3.7 did its job better than v1.2.3.8+ in my opinion.
- Its become more difficult to work with the code with ~1000+ more lines.
- v1.2.3.8+ Organizer was NOT doing its job. Files with special characters were still being ignored. Why keep the changes if the changes are not doing their job? The File Organizer revamp (from v1.2.3.8+ until v1.2.4.1) was more hassle than its worth in my opinion.

Don't worry though, older versions ARE available in the releases tab!

# LinuxPlusGNUToolBox v1.2.4.0

- Updated links in manual and readme.
- Updted readme.

# LinuxPlusGNUToolBox v1.2.3.9

- Hopefully resolved an issue where File Organizer would make directories regardless of whether the file exists or not (antithetical to the intent of my rewrite in v1.2.3.8.) Please note that this is STILL a testing version. Not everything might work as intended in this version!
- When using the Updater tool, the Updater tool will now make a directory called 'LinuxPlusGNUToolBox' (for backups of LinuxPlusGNUToolBox before updating) and move it to '~/Documents' (Current User's Documents Directory.)

# LinuxPlusGNUToolBox v1.2.3.8

- Added more info after using File Organizer.

## Major File Organizer Changes:

### Pros:
- Only does actions if those actions need to be done.
- Saves disk writes.

### Neutral:
- Speed not noticeably effected (in my case.)
- Re-write of the old File Organizer which is STILL available via v1.2.3.7 release.

### Cons:
- Significantly more lines of code. Specifically an addition of ~1000+ lines?

I CAN revert this change if people want the old version back. Or you can use an older version before this change took effect (v1.2.3.7.)

# LinuxPlusGNUToolBox v1.2.3.7

- Updated Change-Log.
- Updated Read-Me.
- Updated Manual.
- Added a new tool called 'fn_brightness_tool' (Xrandr Brightness Tool.) You MIGHT have to configure this tool to set a correct display! If I have time, I MIGHT be adding support for setting a refresh rate and screen size. No promises though.
- Added new Manual Entry for Xrandr Brightness Tool.

# LinuxPlusGNUToolBox v1.2.3.6

- Corrected several spelling mistakes within the Change-Log.
- Updated Change-Log.
- Updated Read-Me.
- Added World Time to ToolBox.
- Rearranged Order of operations for Time (UNIT) Converter (NOT World Time!)
- Updated Manual (Significantly more readable.)
- Removed [Calculator and Checksum Validator] because I was NOT satisfied with their function. Previous Tools ARE STILL AVAILABLE in earlier versions of LinuxPlusGNUToolBox.
- Updated Project Info Webpage.
- Removed Version Numbers everywhere I could find them (excluding The Updater, The Manual and The Main Command Menu.)
- Added Personal Notes to the Manual.

# Updated Code-Base:
    - Renamed fn_automated_bk_tool -> fn_bk_tool
    - Renamed fn_project_info -> fn_manual
    - De-bloat! Significantly simplified the Code-Base by removing A LOT OF LINES! ['~2.1k Lines' in v1.2.3.5 -> '~1k Lines' in v1.2.3.6.] More lines LIKELY CAN be removed!

# v1.2.3.5

- Updated Manual.
- Updated Change-Log.

# v1.2.3.4

- Added weeks in LinuxPlusGNUTimeConverter v0.6! (Not sure how I forgot to make that.) :)
- Working on an updated manual.
- Updated Change-Log.
- Attempting to restructure the Change-Log (for continuity.)
- Issues regarding LinuxPlusGNUTimeConverter v0.5 displaying, "Days:" instead of units that are not days should now be corrected in LinuxPlusGNUTimeConverter v0.6.

# v1.2.3.3

MASSIVE content update! Added new entries to LinuxPlusGNUTimeConverter! :)

The following is now supported in LinuxPlusGNUTimeConverter v0.5:

- Millennia
- Centuries
- Decades
- Years
- Months
- Milliseconds (Millennia and Centuries are not exact as far as I am aware.)
- Restructured output of LinuxPlusGNUTimeConverter v0.5.
- Corrected a little mistake in the Change-Log.

I might be adding new manual entries for this soon. Thanks for checking out my software! I hope you enjoy using it!

# v1.2.3.2

- Removed licensing nags.
- LinuxPlusGNUFileOrganizer v2.5 has been reverted to version 2.1 due to the process unintentionally deleting system files.
- Thinking of removing the calculator and time converter. (I may create my own web apps for these.)
- Thinking of adding user ***inputable variables to LinuxPlusGNUChecksumsValidator so you don't have to configure the script.

# v1.2.3.1

- Added a variable called, "scale" with a value equal to 5 to LinuxPlusGNUCalculator v0.4. This is primarily used to show decimal numbers. Maybe I'll add infinite bc command support using a bash loop? 

# v1.2.3.0

- Addressed and hopefully resolved the issue with LinuxPlusGNUFileOrganizer v2.3s issue regarding .7z file extensions/suffixes in v2.4 of LinuxPlusGNUFileOrganizer.
- Removed spaces between LinuxPlusGNUFileOrganizer name within the LinuxPlusGNUFileOrganizer tool. 

# v1.2.2.9

- Moved some text down a bit after you finish updating LinuxPlusGNUToolBox.sh. 
- Patched an obnoxious bug that would find all files recursively and move them to the directories within the Organized directory. LinuxPlusGNUFileOrganizer v2.3 should be tamed now! 
- A WARNING! Do NOT use LinuxPlusGNUToolBox v1.2.2.8s LinuxPlusGNUFileOrganizer v2.2. It is broken. 

# v1.2.2.8

- Added a new tool called, "LinuxPlusGNUToolBoxUpdater." This tool is used to update LinuxPlusGNUToolBox.
- Added Manual entry for LinuxPlusGNUToolBoxUpdater.
- Changed the input numbers for the command menu.
- The command menu now displays the LinuxPlusGNUToolBox version.

# v1.2.2.7

- Major content update! I decided to completely rewrite LinuxPlusGNUFileOrganizer in order to conserve disk reads and writes. Instead of creating directories for files you may not have, LinuxPlusGNUFileOrganizer will now check if you have files with certain file extensions before making directories for files with those certain file extensions. For example, if you have mp3 files, LinuxPlusGNUFileOrganizer will try to put those mp3 files within the mp3 directory that is inside of the Organized directory. (You no longer run commands you don't need to.) This also increases the overall speed and efficiency of LinuxPlusGNUFileOrganizer! Everything should still remain compatible as far as I am aware. Thus, everything should be upgradable without issue.

- We also have a new universal variable for version checking called, "version_check."
- Updated readme!
- Updated Change-Log!

# v1.2.2.6

- Minor content update. 
- Added date for when backup finishes. 

# v1.2.2.5

- Major content update!
- Updated manual. 
- Updated Change-Log.
- LinuxPlusGNUCalculator now supports calculations that include a decimal point (floating point calculations) thanks to the bc command!

# v1.2.2.4

- We have a new addition to the LinuxPlusGNUToolBox family! Now introducing LinuxPlusGNUCalculator!
- For correct responses with LinuxPlusGNUCalculator, only input ONE calculation at a time.
- Updated licensing link to take you to my project information page! :)
- Added an entry I forgot to add in v1.2.2.3s log entry to v1.2.2.3s log entry. 

# v1.2.2.3

- Added stars to some of the title entries.
- Major content update! 
- Corrected some spelling mistakes.
- Added Days time conversion to LinuxPlusGNUTimeConverter.
- Added Hours time conversion to LinuxPlusGNUTimeConverter.
- Added Minutes time conversion to LinuxPlusGNUTimeConverter.

# v1.2.2.2

- Major content update! 
- Added a menu for the time converter. 
- Added placeholder time converters for time converters I have not worked on yet. 
- Made the manual more readable.
- Updated manual.
- Updated Change-Log.

# v1.2.2.1

- Corrected a spelling mistake.

# v1.2.2

- We have a new addition to the LinuxPlusGNUToolBox family! Now introducing LinuxPlusGNUTimeConverter.
- LinuxPlusGNUTimeConverter is a VERY basic tool used to convert seconds to days, hours and minutes. 
- Currently, LinuxPlusGNUTimeConverter cannot convert days, hours and minutes to seconds. 
- Corrected spelling errors. 
- Updated Manual. 
- Introduced new (experimental) multi-line comment area(s) within LinuxPlusGNUToolBox.

# v1.2.1 

- Patched a bug that would not relaunch LinuxPlusGNUToolBox.sh when asked if you would like to return back to the main menu.

# v1.2

- Changed the name, "LinuxPlusGNUBackupTool" to, "LinuxPlusGNUToolBox" because this new name makes a lot more sense. 
- Added a function that clears the logs then moves the logs to the trash after 29-30 24 hour days. 
- Days are now listed in time until loop within the Linux Plus GNU Automated Backup Tool.
- Updated Change-Log.
- Updated readme.

# v1.1

- Added two new tools! The Linux Plus GNU Checksums Validator and the Linux Plus GNU Automated Backup Tool. Check the manual for more information! :)
- Removed menu entries that no longer had functions. 
- Removed rsync commands. 
- Updated manual. 

# v1.0

- After a process has been successfully completed, you will be asked if you want to return back to the main menu.
- Process now clear themselves after they successfully finish. 
- Updated Rsync command.
- Working on fixing Rsync (absolute paths may break Rsync?) (No guarantees on fixing this.)
- Create log when running the Rsync command.
- You MAY get gibberish errors upon aborting the process. These should not be anything to worry about. 

# v0.9 

- Subtle bug fixes. 
- Added an arrow for an item in the manual.

# v0.8

- Another huge milestone! I removed a bunch of useless code, restructured how the backups work.
- Backups are now separated into, "~/LinuxPlusGNUBackupTool/ISO" for the iso file and, "~/LinuxPlusGNUBackupTool/Backup" for the raw backup files.
- The manual and code-base have been updated as well! 

# v0.7

- MAJOR content update! Added more menu items, more control over your backups, updated manual.
- Backups will no longer backup the ~/Backup folder.
- Checksums ARE properly backed up into ~/Backup/checksums.txt (with an accurate timestamp.)
- Checksums are also validated while the backup process is running. 

# v0.6

- Added .svg support to the LinuxPlusGNUBackupTool.

# v0.5

- Added a manual.
- Edited some characters so that the user experience is a lot more clear. 
- Overall, general improvements. 

# v0.4 

- Merged LinuxPlusGNUFileOrganizer with LinuxPlusGNUBackupTool.
- LinuxPlusGNUBackupTool is now the main repository for both projects.
- LinuxPlusGNUFileOrganizer is now a part of LinuxPlusGNUBackupTool.

# v0.3

- Added links.
- Removed some redundant characters.

# v0.2

- Restructuring everything, now semi-functional. 

# v0.1

- Initial release.

