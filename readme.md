# What is ToolBox?

ToolBox is lightweight and powerful software I've developed to help you with your everyday tasks.

ToolBox currently has its own Updater, 12 and 24 hour Time Table, Xrandr Brightness Tool, Temperature Unit Converter, Time (UNIT) Converter, World Time Tool, Backup Tool, Password Generator, File Organizer and Manual.

I am currently trying to clean up this project! :)

# Known issues

ToolBox is by no means perfect. If you'd like to make positive contributions to this project, feel free to do so! :)

**Time (UNIT) Converter**

- Limited support for numbers with decimal points due to the way BASH operates so expect some margin for error.

**File Organizer**

- A file named, "-Test.png" may NOT work whereas, "Test.png" will likely work.

**ToolBox Updater**

This issue may be resolved upon updating to v1.2.4.5.
- A folder called 'ToolBox' is made in the directory 'ToolBox.sh' is started from when updating instead of moving it to ```'~/Documents/ToolBox.'``` The backed up file might also be moved to ```'~/Documents/ToolBox'``` I am currently unsure.

This issue may be resolved upon updating to v1.2.4.6.
- From the directory where you started 'ToolBox.sh', there may still be version number backups (if you've used the ToolBox Updater.) Backups are supposed to go to ```'~/Documents/ToolBox_BKs/'``` instead of the directory where you started 'ToolBox.sh' from.

# Licensing and donation information:

https://c9x3.neocities.org/

# Changelog

https://gitlab.com/c9x3/ToolBox/-/blob/main/changes.md

# How to run

chmod +x ToolBox.sh
\
./ToolBox.sh

# FileOrganizer supports these file extensions

- webp
- odt
- gif
- jpeg
- jpg
- mov
- mp3
- mp4
- png
- webm
- wav
- ogg
- mid
- midi
- wma
- aif
- m4v
- mkv
- txt
- m4a
- caf
- m4r
- zip
- 7z
- rar
- flac
- jar
- svg
- pdf

# TimeConverter supports the following time conversions

- millennia
- centuries
- decades
- years
- months
- weeks
- days
- hours
- minutes
- seconds
- milliseconds
